#include <iostream>
#include <vector>

using namespace std;

void die(string msg)
{
	cerr << msg << endl;
	exit(1);
}

void alloc_cont(int n, int min, int max)
{
	vector<int> cont;
	int allocated;
	int available;

	if (n < 0 || min < 0 || max < 0) {
		die("-ve entries ? Are you kidding me ?");
	}

	if (!max || min > max) {
		die("set a higher max count");
	}
	
	if (n < min) {
		die("set a lower min count and/or higher n count");
	}

	while (n > 0) {
		if (n >= max) {
			cont.push_back(max);
			n -= max;
		} else {
			cont.push_back(n);
			n = 0; /* n exhausted */
		}
	}

	int& last_cont = cont.back();
	if (last_cont < min) {
		for (auto i = cont.begin(); i != cont.end(); i++) {
			available = *i - min;
			if (available) {
				*i -= available;
				last_cont += available;
			}

			if (last_cont >= min)
				break;
		}
	}

	if (last_cont < min) {
		die("impossible to assign");
	} else {
		cout << cont.size() << " containers used where each has\n";
		for (auto i = cont.begin(); i != cont.end(); i++) {
			cout << *i << endl;
		}
	}
}

int main()
{
	int n_apples;
	int min_apples_per_cont;
	int max_apples_per_cont;

	cout << "Enter num of apples : ";
	cin >> n_apples;

	cout << "Enter min num of apples per container : ";
	cin >> min_apples_per_cont;

	cout << "Enter max num of apples per container : ";
	cin >> max_apples_per_cont;

	alloc_cont(n_apples, min_apples_per_cont, max_apples_per_cont);
}
