#include <stdio.h>
#include <stdlib.h>
#include <string.h>

size_t matrix_m = 10000;
size_t matrix_n = 10000;

/*
 * (i1, j1) always refers to the "from-index" and (i2, j2) refers to the
 * "to-index" of all possible *'ed L's in the matrix
 */
struct largest_L {
	/* (i, j) pair for  "from-index" */
	size_t i1;
	size_t j1;

	/* (i, j) pair for "to-index" */
	size_t i2;
	size_t j2;

	size_t size;
} largest_L_global;

void *zalloc(size_t size)
{
	void *alloc;

	alloc = calloc(size, 1);

	if (!alloc) {
		fprintf(stderr, "malloc failed!");
		exit(EXIT_FAILURE);
	}
	return alloc;
}

static inline size_t get_height(size_t i1,  size_t i2)
{
	return (i2 - i1) + 1;
}

static inline size_t get_width(size_t j1,  size_t j2)
{
	return (j2 - j1) + 1;
}

static size_t get_L_size(size_t i1, size_t j1, size_t i2, size_t j2)
{
	size_t height = get_height(i1, i2);
	size_t length = get_width(j1, j2);

	/* -1 for the common '*' at the base of L */
	return (height > 1 && length > 1) ? (height + length) - 1 : 0;
}

static void set_largest_L_local(char **matrix, size_t m, size_t n,
		size_t i1, size_t j1)
{
	size_t i2, j2, current_L_size;

	if (!(matrix[i1][j1] == '*'))
		return;

	for (i2 = i1 + 1; i2 < m && matrix[i2][j1] == '*'; i2++) {
		for (j2 = j1 + 1; j2 < n && matrix[i2][j2] == '*'; j2++) {
		}
		j2--; /* since j2 must point to a non '*' or out-of-bounds */
		current_L_size = get_L_size(i1, j1, i2, j2);
		if (current_L_size > largest_L_global.size) {
			largest_L_global.size = current_L_size;
			largest_L_global.i1 = i1;
			largest_L_global.j1 = j1;

			largest_L_global.i2 = i2;
			largest_L_global.j2 = j2;
		}
	}
}

static void set_largest_L_global(char **matrix, size_t m, size_t n)
{
	for (size_t i = 0; i < m; i++) {
		for (size_t j = 0; j < n; j++) {
			set_largest_L_local(matrix, m, n, i, j);
		}
	}
}

int main()
{
	char **matrix;
	size_t i, j;

	/* alloc our matrix on the heap, to avoid SO on bigger matrix */
	matrix = zalloc(sizeof(*matrix) * matrix_m);
	for (i = 0; i < matrix_m; i++) {
		matrix[i] = zalloc(sizeof(**matrix) * matrix_n);
		memset(matrix[i], '+', sizeof(**matrix) * matrix_n);
	}

	/* input section */
	for (i = 0, j = 0; i < 100; i++)
		matrix[i][j] = '*';

	for (i = 1, j = 0; i < 200; i++)
		matrix[i][j] = '*';

	for (j = 0, i = 10; j < 100; j++)
		matrix[i][j] = '*';

	for (j = 0, i = 20; j < 200; j++)
		matrix[i][j] = '*';

	/* process and output */
	set_largest_L_global(matrix, matrix_m, matrix_n);
	if (!largest_L_global.size)
		goto done;

	printf("largest L from (%zu, %zu) to (%zu, %zu) of total length %zu "
			"where height = %zu and width = %zu\n",
			largest_L_global.i1,
			largest_L_global.j1,
			largest_L_global.i2,
			largest_L_global.j2,
			largest_L_global.size,
			get_height(largest_L_global.i1, largest_L_global.i2),
			get_width(largest_L_global.j1, largest_L_global.j2)
			);

done:
	return EXIT_SUCCESS;
}
